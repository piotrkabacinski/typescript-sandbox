(function( SystemJS ) {

  SystemJS.config({
      baseURL: './js',
      packages: {
        './js': {
          defaultExtension: 'js'
        }
      }
    });

  SystemJS.import('app');

})( SystemJS );
