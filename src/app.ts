import { foo } from './foo';

let a = (arg: string): string => {
  return `Message: ${arg}`;
}

console.log(a(foo));
